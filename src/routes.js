import React from 'react';
import { View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

// import { Container } from './styles';
import Home from './pages/home'
import Portal from './pages/portal'

const Stack = createStackNavigator();

export default function Router() {
  return (
    <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Portal" component={Portal} />
    </Stack.Navigator>
  );
}
