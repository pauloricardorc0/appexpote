import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

// import { Container } from './styles';

export default function Portal() {
  return ( 
      <View style={styles.Container}>
          <Text style={styles.texto}>Tela de Portal</Text>
      </View>
  );
}

const styles = StyleSheet.create({
    Container:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    texto:{
        color: "#333",
        fontSize: 20
    }
})