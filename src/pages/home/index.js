import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

// import { Container } from './styles';

export default function Home({ navigation }) {
    function SegundaTela(){
        navigation.navigate('Portal')
    }

  return ( 
      <View style={styles.Container}>
          <Text style={styles.texto}>Tela de Home</Text>
          <Button title="Segunda tela" onPress={(SegundaTela)} />
      </View>
  );
}

const styles = StyleSheet.create({
    Container:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    texto:{
        color: "#333",
        fontSize: 20
    }
})