import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Router from './src/routes'

// import { Container } from './styles';

export default function Home() {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  );
}
